const express = require('express');
const app = express();
const cors = require('cors');
const Port = process.env.PORT || 3000;
const routes = require("./routes/index.router");

require('dotenv').config({path:'./variable.env'});
///// database //////
const connectDB = require ('./config/database');
connectDB();
///// middlewares ////
app.use(cors());
app.use(express.json({extended:true}));
//// router ////
app.use(routes);

app.listen(Port,()=>{
    console.log('Servidor iniciado',Port);
})