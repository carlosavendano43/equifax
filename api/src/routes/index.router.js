const { Router } = require('express');
const router = Router();
const auth = require("../components/auth/auth.route");

router.use('/auth',auth);

module.exports = router;