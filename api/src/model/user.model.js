const { Schema, model } = require('mongoose');

const userSchema = new Schema({
    email:{
        type: String,
        trim: true,
        required: true,
        unique: true,
        lowercase: true
    },
    password:{
        type: String,
        trim: true,
        required: true,
    },

},{
    timestamps:true,
})


module.exports = model('User',userSchema);