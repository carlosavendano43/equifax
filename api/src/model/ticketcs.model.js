const { Schema, model } = require('mongoose');

const userSchema = new Schema({
    id_user:{
        type: String,
        trim: true,
        required: true,
        lowercase: true
    },
    title:{
        type: String,
        required: true,
    },
    description:{
        type: String,
        required: true,
    },
    status:{
        type: String,
        required: true,
    },
},{
    timestamps:true,
})


module.exports = model('User',userSchema);