const { Router } = require('express');
const router = Router();

router.get('/tickets',AuthController.SignIn);
router.post('/tickets/register',AuthController.SignUp);
router.put('/tickets/modify',AuthController.SignUp);
router.delete('/tickets/delete/:id',AuthController.SignUp);


module.exports = router;