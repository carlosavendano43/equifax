const AuthServices = require("./auth.service");
class AuthController {
    static SignIn = async(req,res) => {

    }

    static SignUp = async(req,res) => {
        try {
            const response = await AuthServices.SignUp(req);
            res.status(200).json(response); 
        }catch (e){
            console.log(e);
            res.status(500).json(e);
        }
    }
}

module.exports = AuthController;