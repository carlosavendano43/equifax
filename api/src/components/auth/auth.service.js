
const User = require("../../model/user.model");
const { hashPassword, checkPassword } = require("../../helpers/bcrypt.helper");
class AuthServices {
    static SignIn = async(req) => {
        try {
            const { email, contrasena } = req.body;
            const password = hashPassword(contrasena);
            
            const userModel = new User({email,password});

            const response = await userModel.save();

            return response;
        } catch (error){
            return error;
        }
    }

    static SignUp = async(req) => {
        try {
            const { email, contrasena } = req.body;
            const user = await User.findOne({email})

            if(user) throw {"code":"fail","message":"email existente"}
        
            const password = hashPassword(contrasena);
       
            const userModel = new User({email,password});
            await userModel.save();

            return {"code":"ok","message":"registro exitoso"};
        } catch (error){
            throw error;
        }
    }
}

module.exports = AuthServices;